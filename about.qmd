# A propos

<br/>

Ce site a été produit en utilisant la technologie [Quarto](https://quarto.org/) par l'équipe EcoElec. 

Son code source est [librement consultable](https://forgemia.inra.fr/ecoelec/communication/ecoelec).




