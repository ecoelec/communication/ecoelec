---
title: "Ressources"
---

## Présentations 

Présentation faite en octobre et décembre 2021 :

- [https://ecoelec.pages.mia.inra.fr/communication/slides/presentation_phase1/](https://ecoelec.pages.mia.inra.fr/communication/slides/presentation_phase1/)

## Ressources produites

- Guide de mise en route des mesures dans un laboratoire : [lien site](https://ecoelec.pages.mia.inra.fr/communication/guide-mesures/) ou [lien pdf](https://ecoelec.pages.mia.inra.fr/communication/guide-mesures/Projet-EcoElec-br-Guide-de-mise-en-route-br-Mesures-d-%C3%A9lectricit%C3%A9-br--br-.pdf)

- Code d'exploitation des résultats : Package R disponible [ici](https://forgemia.inra.fr/ecoelec/ecoelecAnalysis). Voir la vignette associée : [https://ecoelec.pages.mia.inra.fr/ecoelecAnalysis/index.html](https://ecoelec.pages.mia.inra.fr/ecoelecAnalysis/index.html)

- Appli Shiny : TODO

- Code source eSandMan

- Guide de bonnes pratiques (à rédiger)
