# Code pour le site web du projet EcoElec

[Lien vers le site](https://ecoelec.pages.mia.inra.fr/communication/ecoelec)

# Pour lancer en local 

1. Installer `quarto`
```
https://docs.posit.co/resources/install-quarto/
```
2. Lancer `quarto render`dans le terminal
3. Le code du site web est dans le dossier public/
